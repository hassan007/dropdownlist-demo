﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dropdownlist_demo.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ModelNo { get; set; }
        public string CountryofOrigin { get; set; }
    }
}