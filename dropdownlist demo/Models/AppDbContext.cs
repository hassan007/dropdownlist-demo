﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace dropdownlist_demo.Models
{
    public class AppDbContext:DbContext
    {
        public AppDbContext() : base() { }

        public System.Data.Entity.DbSet<dropdownlist_demo.Models.Product> Products { get; set; }
    }
}